package Model;

public abstract class Conta {

	private double saldo;
	
	public Conta (double saldo){
		this.saldo = saldo;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public void deposita(double deposito){
		this.saldo += deposito;
	}
	
	public void saca (double valor){
		this.saldo -=valor;
	}
	
	abstract void atualiza(double taxaSelic);

}
